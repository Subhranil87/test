package com.mercury.test;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
//import org.sikuli.script.Pattern;
//import com.gargoylesoftware.htmlunit.javascript.host.Screen;

public class MethodRepository {
	WebDriver driver;
    public void mercuryAppLaunch() throws InterruptedException, IOException, AWTException, FindFailed
   {
	System.setProperty("webdriver.chrome.driver", "E:\\AutomationTesting\\Tools\\chromedriver.exe");	
	WebDriver driver=new ChromeDriver();
	Actions action = new Actions(driver);
	driver.get("http://newtours.demoaut.com/");
	driver.manage().window().maximize();
	//Thread.sleep(5000);
	//driver.wait();
    //driver.quit();
	//WebElement register=driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[2]/td/table/tbody/tr/td[2]/a"));
	//register.click();
	//WebElement uName=driver.findElement(By.name("userName"));
	WebElement uName=driver.findElement(By.xpath("//input[@name='userName']"));
	BufferedReader Test=new BufferedReader(new FileReader("./Testdata.properties"));
	Properties prop=new Properties();
	prop.load(Test);
	String uname= prop.getProperty("Username");
	uName.sendKeys(uname);
	//WebElement pass=driver.findElement(By.name("password"));
	WebElement pass=driver.findElement(By.xpath("//input[@name='password']"));
	String pass1=prop.getProperty("Password");
	pass.sendKeys(pass1);
	WebElement element=driver.findElement(By.name("login"));
	action.moveToElement(element).click().perform();
	File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(scrFile, new File("E:\\AutomationTesting\\Screenshots\\1.jpeg"));
	WebElement dropdown=driver.findElement(By.name("fromPort"));
	Select se=new Select(dropdown);
	se.selectByIndex(4);
	
	//WebElement logIn=driver.findElement(By.name("login"));
	//WebElement logIn=driver.findElement(By.xpath("//input[@name='login']"));
	//logIn.click();
	/*Robot R=new Robot();
	R.keyPress(KeyEvent.VK_TAB);
	R.keyRelease(KeyEvent.VK_TAB);
	R.keyPress(KeyEvent.VK_ENTER);
	R.keyRelease(KeyEvent.VK_ENTER);
	Screen s1=new Screen();
    Pattern p1 = new Pattern("E:\\Capture1.png");
	s1.click(p1);
	Runtime.getRuntime().exec("C:\\Users\\Rony\\Documents\\19th May.exe");
	File scrFile2 = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	FileUtils.copyFile(scrFile2, new File("E:\\AutomationTesting\\Screenshots\\2.jpeg"));*/
   }

}



